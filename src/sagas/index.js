import { fork } from 'redux-saga/effects';
import {
  indexEmployeeOnEnter,
  showEmployeeOnEnter,
  listenToCreateEmployee,
} from './employee-saga';

export default function * root () {

  yield fork(indexEmployeeOnEnter);
  yield fork(showEmployeeOnEnter);
  yield fork(listenToCreateEmployee);
}
