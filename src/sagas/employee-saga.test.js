import { call, put } from 'redux-saga/effects';
import { request } from '../lib/api';
import history from '../lib/history';
import {
  employeesMock,
  employeeMock,
  responseEmployeesMock,
  responseEmployeeMock,
  getEmployeesMock,
  getEmployeeMock,
  postEmployeeMock,
} from '../redux-test-helper';
import {
  employeeIndexLoadSuccess,
  employeeIndexLoadFail,
  employeeShowLoadSuccess,
  employeeShowLoadFail,
  employeeCreateSuccess,
  employeeCreateFail,
} from '../redux/actions/employee-actions';
import {
  createEmployee,
  indexEmployee,
  showEmployee,
} from './employee-saga';

let actualYield;
let expectedYield;

describe('employee saga', () => {
  // INDEX
  describe('indexEmployee ', () => {
    describe('when data is fetched successfully', () => {
      let iterator = indexEmployee();
      it('performs and API call to get the employees list', () => {
        actualYield = iterator.next().value;
        expectedYield = call(request, getEmployeesMock);
        expect(actualYield).toEqual(expectedYield);
      });
      it('stores employees list in the state', () => {
        actualYield = iterator.next(responseEmployeesMock).value;
        expectedYield = put(employeeIndexLoadSuccess(employeesMock));
        expect(actualYield).toEqual(expectedYield);
      });
      it('it should be done', () => {
        expect(iterator.next().done).toEqual(true);
      });
    });
    describe('when data is not fetched successfully', () => {
      let iterator = indexEmployee();
      it('performs and API call to get the employees list', () => {
        actualYield = iterator.next().value;
        expectedYield = call(request, getEmployeesMock);
        expect(actualYield).toEqual(expectedYield);
      });
      it('triggers employeeIndexLoadFail action', () => {
        actualYield = iterator.next().value;
        expectedYield = put(employeeIndexLoadFail(`Cannot read property 'data' of undefined`));
        expect(actualYield).toEqual(expectedYield);
      });
      it('it should be done', () => {
        expect(iterator.next().done).toEqual(true);
      });
    });
  });

  // SHOW
  describe('showEmployee ', () => {
    describe('when data is fetched successfully', () => {
      let iterator = showEmployee(employeeMock.id);
      it('performs and API call to get the employee entity', () => {
        actualYield = iterator.next().value;
        expectedYield = call(request, getEmployeeMock);
        expect(actualYield).toEqual(expectedYield);
      });
      it('stores employees list in the state', () => {
        actualYield = iterator.next(responseEmployeeMock).value;
        expectedYield = put(employeeShowLoadSuccess(employeeMock));
        expect(actualYield).toEqual(expectedYield);
      });
      it('redirects to employee show page', () => {
        actualYield = iterator.next().value;
        expectedYield = call(history.push, `/employee/${employeeMock.id}`);
        expect(actualYield).toEqual(expectedYield);
      });
      it('it should be done', () => {
        expect(iterator.next().done).toEqual(true);
      });
    });
    describe('when data is not fetched successfully', () => {
      let iterator = showEmployee(employeeMock.id);
      it('performs and API call to get the employee entity', () => {
        actualYield = iterator.next().value;
        expectedYield = call(request, getEmployeeMock);
        expect(actualYield).toEqual(expectedYield);
      });
      it('triggers employeeIndexLoadFail action', () => {
        actualYield = iterator.next().value;
        expectedYield = put(employeeShowLoadFail(`Cannot read property 'data' of undefined`));
        expect(actualYield).toEqual(expectedYield);
      });
      it('it should be done', () => {
        expect(iterator.next().done).toEqual(true);
      });
    });
  });

  // CREATE
  describe('createEmployee ', () => {
    describe('when create is successfull', () => {
      let iterator = createEmployee(employeeMock);
      it('performs and API call to create employee', () => {
        actualYield = iterator.next().value;
        expectedYield = call(request, postEmployeeMock);
        expect(actualYield).toEqual(expectedYield);
      });
      it('triggers employeeShowLoadSuccess action', () => {
        actualYield = iterator.next(responseEmployeeMock).value;
        expectedYield = put(employeeShowLoadSuccess(employeeMock));
        expect(actualYield).toEqual(expectedYield);
      });
      it('triggers employeeCreateSuccess action', () => {
        actualYield = iterator.next(responseEmployeeMock).value;
        expectedYield = put(employeeCreateSuccess(`You have successfully created ${responseEmployeeMock.data.name}!`));
        expect(actualYield).toEqual(expectedYield);
      });
      it('redirects to employee show page', () => {
        actualYield = iterator.next().value;
        expectedYield = call(history.push, `/employee/${employeeMock.id}`);
        expect(actualYield).toEqual(expectedYield);
      });
      it('it should be done', () => {
        expect(iterator.next().done).toEqual(true);
      });
    });
    describe('when create is not successfull', () => {
      let iterator = createEmployee(employeeMock);
      it('performs and API call to create employee', () => {
        actualYield = iterator.next().value;
        expectedYield = call(request, postEmployeeMock);
        expect(actualYield).toEqual(expectedYield);
      });
      it('triggers employeeCreateFail action', () => {
        actualYield = iterator.next().value;
        expectedYield = put(employeeCreateFail(`Cannot read property 'data' of undefined`));
        expect(actualYield).toEqual(expectedYield);
      });
      it('it should be done', () => {
        expect(iterator.next().done).toEqual(true);
      });
    });
  });
});
