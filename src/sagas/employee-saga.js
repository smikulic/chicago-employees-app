import {
  call,
  put,
  takeEvery,
  take,
} from 'redux-saga/effects';
import { request } from '../lib/api';
import history from '../lib/history';
import {
  employeeIndexLoadSuccess,
  employeeIndexLoadFail,
  employeeShowLoadSuccess,
  employeeShowLoadFail,
  employeeCreateSuccess,
  employeeCreateFail,
} from '../redux/actions/employee-actions';

// INDEX
export function* indexEmployee() {
  try {
     const response = yield call(request, {
       url: 'https://dt-interviews.appspot.com/',
       method: 'GET',
      });
      yield put(employeeIndexLoadSuccess(response.data));
  } catch (e) {
    yield put(employeeIndexLoadFail(e.message));
  }
}

export function* indexEmployeeOnEnter() {
  yield takeEvery('employee/INDEX_ENTER', indexEmployee);
}

// SHOW
export function* showEmployee(employeeId) {
  try {
     const response = yield call(request, {
       url: `https://dt-interviews.appspot.com/${employeeId}`,
       method: 'GET',
      });
     yield put(employeeShowLoadSuccess(response.data));
     yield call(history.push, `/employee/${employeeId}`);
  } catch (e) {
    yield put(employeeShowLoadFail(e.message));
  }
}

export function* showEmployeeOnEnter() {
  while (true) {
    const { payload } = yield take('employee/SHOW_ENTER');
    if (payload) {
      yield call(showEmployee, payload);
    }
  }
}

// CREATE
export function* createEmployee(payload) {
  try {
    const response = yield call(request, {
      url: 'https://dt-interviews.appspot.com/',
      method: 'POST',
      payload,
     });
     yield put(employeeShowLoadSuccess(response.data));
     yield put(employeeCreateSuccess(`You have successfully created ${response.data.name}!`));
     yield call(history.push, `/employee/${response.data.id}`);
  } catch (e) {
    yield put(employeeCreateFail(e.message));
  }
}

export function* listenToCreateEmployee() {
  while (true) {
    const { payload } = yield take('employee/CREATE');
    if (payload) {
      yield call(createEmployee, payload);
    }
  }
}
