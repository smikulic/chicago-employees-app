import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import TextField from '@material-ui/core/TextField';

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit,
  },
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: theme.palette.text.secondary,
  },
  title: {
    flex: '0 0 auto',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
});

class EnhancedTableToolbar extends Component {
  filterBy(event) {
    this.props.filterBy(event.target.value);
  }

  render() {
    const { classes } = this.props;
  
    return (
      <Toolbar
        className={classNames(classes.root)}
      >
        <div className={classes.title}>
          <Typography variant="title" id="tableTitle">
            Employees
          </Typography>
        </div>
        <div className={classes.spacer} />
        <div className={classes.actions}>
          <Tooltip title="Filter by department">
            <TextField
              id="search"
              label="Search department"
              type="search"
              className={classes.textField}
              margin="normal"
              onChange={this.filterBy.bind(this)}
            />
          </Tooltip>
        </div>
      </Toolbar>
    );
  }
};

EnhancedTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  filterBy: PropTypes.func.isRequired,
};

export default withStyles(toolbarStyles)(EnhancedTableToolbar);
