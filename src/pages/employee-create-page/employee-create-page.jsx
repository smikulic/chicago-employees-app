import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Snackbar from '@material-ui/core/Snackbar';
import Notification from '../../patterns/molecules/notification';
import { PaperStyle, RootStyle, TitleStyle } from '../../lib/styles';

const styles = theme => ({
  root: RootStyle,
  paper: PaperStyle(theme),
  title: TitleStyle(theme),
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  innerGrid: {
    marginLeft: 'auto',
    marginRight: 'auto',
  },
});

class EmployeeCreatePage extends Component {
  constructor(props) {
    super(props);

    this.handleOnClick = this.handleOnClick.bind(this);

    this.state = {
      name: '',
      job_titles: '',
      employee_annual_salary: '',
      department: '',
      notification: false,
    }
  }

  handleOnClick(event) {
    event.preventDefault();
    const { name, job_titles, employee_annual_salary, department } = this.state;

    if (name && job_titles && employee_annual_salary && department) {
      this.props.employeeCreate({
        name,
        job_titles,
        employee_annual_salary: parseFloat(employee_annual_salary),
        department,
      });
    } else {
      this.setState({ notification: true });
    }
  }

  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ notification: false });
  };

  onDepartmentChange = event => this.setState({ department: event.target.value });
  onJobTitleChange = event => this.setState({ job_titles: event.target.value });
  onSalaryChange = event => this.setState({ employee_annual_salary: event.target.value });
  onNameChange = event => this.setState({ name: event.target.value });

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Grid item xs={12} sm={6} className={classes.innerGrid}>
                <Typography variant="title" className={classes.title}>
                Add New Employee
                </Typography>
                <TextField
                  required
                  id="name"
                  label="Name"
                  className={classes.textField}
                  margin="normal"
                  fullWidth
                  onChange={this.onNameChange}
                />
                <TextField
                  required
                  id="job_titles"
                  label="Job Title"
                  className={classes.textField}
                  margin="normal"
                  fullWidth
                  onChange={this.onJobTitleChange}
                />
                <TextField
                  required
                  id="employee_annual_salary"
                  label="Employee Annual Salary"
                  className={classes.textField}
                  margin="normal"
                  fullWidth
                  onChange={this.onSalaryChange}
                />
                <TextField
                  required
                  id="department"
                  label="Department"
                  className={classes.textField}
                  margin="normal"
                  fullWidth
                  onChange={this.onDepartmentChange}
                />
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleOnClick}
                >
                  Create
                </Button>
              </Grid>
            </Paper>
            <Snackbar
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={this.state.notification}
              autoHideDuration={5000}
              onClose={this.handleClose}
            >
              <Notification
                variant="warning"
                message="You need to fill in all the fields!"
                onClose={this.handleClose}
              />
            </Snackbar>
          </Grid>
        </Grid>
      </div>
    );
  }
}

EmployeeCreatePage.propTypes = {
  classes: PropTypes.object.isRequired,
  employeeCreate: PropTypes.func.isRequired,
};

export default withStyles(styles)(EmployeeCreatePage);
