import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import EnhancedTableToolbar from '../../patterns/molecules/enhanced-table-toolbar';
import EnhancedTableHead from '../../patterns/molecules/enhanced-table-head';
import { PaperStyle, RootStyle, TableStyle } from '../../lib/styles';

const styles = theme => ({
  root: RootStyle,
  paper: PaperStyle(theme),
  table: TableStyle,
  tableWrapper: {
    overflowX: 'auto',
  },
  clickable: {
    cursor: 'pointer',
  },
  link: {
    textDecoration: 'none',
  },
});

class EmployeesPage extends Component {
  constructor(props) {
    super(props);

    this.handleKeyPress = this.handleKeyPress.bind(this);

    this.state = {
      selected: props.employeeSelected || '',
      data: props.employees,
      page: 0,
      rowsPerPage: 5,
      counter: props.employeeSelected ? (props.employeeSelected.id - 1) : 0,
    };
  }

  handleKeyPress(event) {
    const DOWN_ARROW = 40;
    const UP_ARROW = 38;
    const ENTER = 13;
    const { employees } = this.props;
    const { selected, counter } = this.state;
    let tempCounter = counter;
    let counterFromLast = employees.length;

    if(event.keyCode === DOWN_ARROW) {
      (!selected) ? tempCounter = 0 : tempCounter++;
      
      if (employees && tempCounter >= 0) {
        this.setState({ selected: employees[tempCounter], counter: tempCounter });
      }
    }
    if(event.keyCode === UP_ARROW) {
      (!selected) ? tempCounter = counterFromLast : tempCounter--;

      if (employees && tempCounter >= 0) {
        this.setState({ selected: employees[tempCounter], counter: tempCounter });
      }
    }

    if(event.keyCode === ENTER) {
      this.props.employeeShowEnter(selected.id);
    }
  }

  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyPress);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyPress);
  }

  handleOnNameClick = employeeId => this.props.employeeShowEnter(employeeId);
  handleChangePage = (event, page) => this.setState({ page });
  handleChangeRowsPerPage = event => this.setState({ rowsPerPage: event.target.value });
  isSelected = id => this.state.selected.id === id;

  render() {
    const { classes, employees, employeeFilterBy } = this.props;
    const { rowsPerPage, page } = this.state;

    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
          <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Grid container alignItems="center" direction="row" justify="flex-end">
              <Link to="/employee/create" className={classes.link}>
                <Button variant="contained" color="primary">Add Employee</Button>
              </Link>
            </Grid>
            <EnhancedTableToolbar filterBy={(filter) => employeeFilterBy(filter, 'department')} />
            <div className={classes.tableWrapper}>
              <Table
                className={classes.table}
                aria-labelledby="employees"
                onKeyDown={this.handleKeyPress}
              >
                <EnhancedTableHead rowCount={employees.length} />
                <TableBody>
                  {employees && (
                    employees.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map(n => {
                      const isSelected = this.isSelected(n.id);
                      return (
                        <TableRow
                          hover
                          role="checkbox"
                          aria-checked={isSelected}
                          tabIndex={-1}
                          key={n.id}
                          selected={isSelected}
                        >
                          <TableCell padding="checkbox">
                            <Checkbox
                              checked={isSelected}
                              onClick={this.handleOnNameClick.bind(this,n.id)}
                            />
                          </TableCell>
                          <TableCell
                            component="th"
                            scope="row"
                            padding="none"
                            className={classes.clickable}
                            onClick={this.handleOnNameClick.bind(this,n.id)
                          }>
                            {n.name}
                          </TableCell>
                          <TableCell
                            component="th"
                            scope="row"
                            padding="none"
                          >
                            {n.job_titles}
                          </TableCell>
                        </TableRow>
                      );
                    }))}
                </TableBody>
              </Table>
            </div>
            <TablePagination
              component="div"
              count={employees.length}
              rowsPerPage={rowsPerPage}
              page={page}
              backIconButtonProps={{
                'aria-label': 'Previous Page',
              }}
              nextIconButtonProps={{
                'aria-label': 'Next Page',
              }}
              onChangePage={this.handleChangePage}
              onChangeRowsPerPage={this.handleChangeRowsPerPage}
            />
          </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

EmployeesPage.propTypes = {
  classes: PropTypes.object.isRequired,
  employees: PropTypes.array.isRequired,
  employeeFilterBy: PropTypes.func.isRequired,
};

export default withStyles(styles)(EmployeesPage);
