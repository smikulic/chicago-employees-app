import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import isEmpty from 'is-empty';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Snackbar from '@material-ui/core/Snackbar';
import Notification from '../../patterns/molecules/notification';
import history from '../../lib/history';
import { PaperStyle, RootStyle, TableStyle, TitleStyle } from '../../lib/styles';

const styles = theme => ({
  root: RootStyle,
  paper: PaperStyle(theme),
  title: TitleStyle(theme),
  table: TableStyle,
});

class EmployeePage extends Component {
  constructor(props) {
    super(props);

    this.handleKeyPress = this.handleKeyPress.bind(this);
    
    this.state = {
      notification: !!props.notificationMessage,
    }
  }

  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ notification: false });
  };

  handleKeyPress(event) {
    const DOWN_ARROW = 40;
    const UP_ARROW = 38;
    const ENTER = 13;
    const { employee, employees } = this.props;
    const nextEmployee = employees[employee.id];
    const currentEmployee = employees[employee.id - 1];
    const previousEmployee = employees[employee.id - 2];
    
    if(event.keyCode === DOWN_ARROW && !isEmpty(nextEmployee)) {
      this.props.employeeShowEnter(nextEmployee.id);
    }

    if(event.keyCode === UP_ARROW && !isEmpty(previousEmployee)) {
      this.props.employeeShowEnter(previousEmployee.id);
    }

    if(event.keyCode === ENTER) {
      this.props.employeeSelect(currentEmployee);
      history.push('/employees');
    }
  }

  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyPress);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyPress);
  }

  render() {
    const { classes, employee, notificationMessage } = this.props;
    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Typography variant="title" className={classes.title}>
                Employee Information
              </Typography>
              <Table className={classes.table}>
                <TableBody>
                  { !isEmpty(employee) && (
                    Object.keys(employee).map(attribute => {
                      return (
                        <TableRow key={attribute}>
                          <TableCell component="th" scope="row">{attribute}</TableCell>
                          <TableCell numeric>{employee[attribute]}</TableCell>
                        </TableRow>
                      );
                    })
                  )}
                </TableBody>
              </Table>
            </Paper>
            <Snackbar
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={this.state.notification}
              autoHideDuration={5000}
              onClose={this.handleClose}
            >
              <Notification
                variant="success"
                message={notificationMessage}
                onClose={this.handleClose}
              />
            </Snackbar>
          </Grid>
        </Grid>
      </div>
    );
  }
}

EmployeePage.propTypes = {
  classes: PropTypes.object.isRequired,
  employee: PropTypes.object.isRequired,
  employees: PropTypes.array.isRequired,
  notificationMessage: PropTypes.string,
};

export default withStyles(styles)(EmployeePage);
