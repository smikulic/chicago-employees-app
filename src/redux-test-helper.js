export const ACTION_UNKNOWN = {
  type: 'UNKNOWN',
};

export const employeeMock = {
  id: '1',
  name: 'a',
  job_titles: 'Professor',
  employee_annual_salary: '133700',
  department: 'Awesome',
};

export const employeeMock2 = {
  id: '2',
  name: 'b',
  job_titles: 'Professor',
  employee_annual_salary: '133700',
  department: 'Not So Awesome',
};

export const employeesMock = [employeeMock, employeeMock2];

export const responseEmployeesMock = {
  data: employeesMock,
};

export const responseEmployeeMock = {
  data: employeeMock,
};

export const getEmployeesMock = {
  url: 'https://dt-interviews.appspot.com/',
  method: 'GET',
}

export const getEmployeeMock = {
  url: `https://dt-interviews.appspot.com/${employeeMock.id}`,
  method: 'GET',
}

export const postEmployeeMock = {
  url: 'https://dt-interviews.appspot.com/',
  method: 'POST',
  payload: employeeMock,
}
