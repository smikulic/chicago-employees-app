import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, Route, Switch } from 'react-router-dom';
import createSagaMiddleware from 'redux-saga';
import root from './sagas/index';
import rootReducer from './redux/reducers';
import registerServiceWorker from './registerServiceWorker';
import { employeeIndexEnter } from './redux/actions/employee-actions';
import EmployeeIndexPageContainer from './redux/containers/employee-index-page-container';
import EmployeeShowPageContainer from './redux/containers/employee-show-page-container';
import EmployeeCreatePageContainer from './redux/containers/employee-create-page-container';
import DashboardPage from './pages/dashboard-page';
import App from './App';
import history from './lib/history';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(sagaMiddleware),
);

sagaMiddleware.run(root);

render(
  <Provider store={store}>
    <Router history={history}>
      <App>
        <Switch>
          <Route exact path='/' component={DashboardPage} />
          <Route path='/employees' component={EmployeeIndexPageContainer} onEnter={store.dispatch(employeeIndexEnter())} />
          <Route path='/employee/create' component={EmployeeCreatePageContainer} />
          <Route path='/employee/:id' component={EmployeeShowPageContainer} />
        </Switch>
      </App>
    </Router>
  </Provider>,
  document.getElementById('root'));
registerServiceWorker();
