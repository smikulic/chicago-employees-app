import { ACTION_UNKNOWN, employeeMock, employeesMock } from '../../redux-test-helper';
import { employee as reducer, initialState } from './employee-reducer';
import {
  employeeIndexLoadSuccess,
  employeeShowLoadSuccess,
  employeeCreateSuccess,
  employeeFilterBy,
  employeeSelect,
} from '../actions/employee-actions';

const stateMock = {
  appState: {
    employees: employeesMock,
    filteredEmployees: [employeesMock[0]],
    employee: employeeMock,
    employeeSelected: employeeMock.id,
    notificationMessage: 'You have successfully created Mock!',
  },
};

describe('employee reducer', () => {
  describe('with no given state and an unkown action', () => {
    it('returns the initial state', () => {
      expect(reducer(undefined, ACTION_UNKNOWN)).toEqual(initialState);
    });
  });
  describe('with a given state and an unkown action', () => {
    it('returns the given state', () => {
      const expectedState = stateMock;
      const currentState = stateMock;
      expect(reducer(currentState, ACTION_UNKNOWN)).toEqual(expectedState);
    });
  });
  describe('with no given state and a employeeIndexLoadSuccess action', () => {
    it('returns the state with loaded employees list', () => {
      const action = employeeIndexLoadSuccess(employeesMock);
      const expectedState = {
        ...initialState,
        employees: employeesMock,
      };
      const currentState = initialState;
      expect(reducer(currentState, action)).toEqual(expectedState);
    });
  });
  describe('with no given state and a employeeShowLoadSuccess action', () => {
    it('returns the state with loaded employee entity', () => {
      const action = employeeShowLoadSuccess(employeeMock);
      const expectedState = {
        ...initialState,
        employee: employeeMock,
      };
      const currentState = initialState;
      expect(reducer(currentState, action)).toEqual(expectedState);
    });
  });
  describe('with no given state and a employeeSelect action', () => {
    it('returns the state with selected employee', () => {
      const action = employeeSelect(employeeMock.id);
      const expectedState = {
        ...initialState,
        employeeSelected: employeeMock.id,
      };
      const currentState = initialState;
      expect(reducer(currentState, action)).toEqual(expectedState);
    });
  });
  describe('with a given employees state and a employeeFilterBy action', () => {
    it('returns the state with filtered employees list', () => {
      const action = employeeFilterBy(employeesMock[0].department, 'department');
      const expectedState = {
        ...initialState,
        employees: employeesMock,
        filteredEmployees: [employeesMock[0]],
      };
      const currentState = {
        ...initialState,
        employees: employeesMock,
      };
      expect(reducer(currentState, action)).toEqual(expectedState);
    });
  });
  describe('with a given employee state and a employeeCreateSuccess action', () => {
    it('returns the state with notification message', () => {
      const action = employeeCreateSuccess(stateMock.appState.notificationMessage);
      const expectedState = {
        ...initialState,
        employee: employeeMock,
        notificationMessage: stateMock.appState.notificationMessage,
      };
      const currentState = {
        ...initialState,
        employee: employeeMock,
      };
      expect(reducer(currentState, action)).toEqual(expectedState);
    });
  });
});
