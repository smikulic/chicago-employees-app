export const initialState = {
  employees: [],
  filteredEmployees: [],
  employee: {},
  employeeSelected: '',
  notificationMessage: '',
};

export const employee = (state = initialState, action) => {
  switch (action.type) {
    case 'employee/INDEX_LOAD_SUCCESS':
      return {
        ...state,
        employees: [
          ...action.payload,
        ],
      };
    case 'employee/SHOW_LOAD_SUCCESS':
      return {
        ...state,
        employee: {
          ...action.payload,
        },
      };
    case 'employee/SELECT':
      return {
        ...state,
        employeeSelected: action.payload,
      };
    case 'employee/FILTER_BY':
      const { filter, category } = action;
      return {
        ...state,
        filteredEmployees: [
          ...state.employees.filter(item => item[category] === filter),
        ],
      };
    case 'employee/CREATE_SUCCESS':
      return {
        ...state,
        notificationMessage: action.message,
      };
    default:
      return state;
  }
}

export default employee;
