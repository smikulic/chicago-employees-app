// INDEX
export const employeeIndexEnter = () => ({
  type: 'employee/INDEX_ENTER',
})

export const employeeIndexLoadSuccess = (payload) => ({
  type: 'employee/INDEX_LOAD_SUCCESS',
  payload,
})

export const employeeIndexLoadFail = (message) => ({
  type: 'employee/INDEX_LOAD_FAIL',
  message,
})

export const employeeFilterBy = (filter, category) => ({
  type: 'employee/FILTER_BY',
  filter,
  category,
})

// SHOW
export const employeeShowEnter = (payload) => ({
  type: 'employee/SHOW_ENTER',
  payload,
})

export const employeeShowLoadSuccess = (payload) => ({
  type: 'employee/SHOW_LOAD_SUCCESS',
  payload,
})

export const employeeShowLoadFail = (message) => ({
  type: 'employee/SHOW_LOAD_FAIL',
  message,
})

export const employeeSelect = (payload) => ({
  type: 'employee/SELECT',
  payload,
})

// CREATE
export const employeeCreate = (payload) => ({
  type: 'employee/CREATE',
  payload,
})

export const employeeCreateSuccess = (message) => ({
  type: 'employee/CREATE_SUCCESS',
  message,
})

export const employeeCreateFail = (message) => ({
  type: 'employee/CREATE_FAIL',
  message,
})
