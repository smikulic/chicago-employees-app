import { connect } from 'react-redux';
import {
  employeeShowEnter,
  employeeSelect,
} from '../actions/employee-actions';
import EmployeeShowPage from '../../pages/employee-show-page';

const mapStateToProps = state => ({
  employee: state.appState.employee,
  employees: state.appState.employees,
  notificationMessage: state.appState.notificationMessage,
})

const mapDispatchToProps = dispatch => ({
  employeeShowEnter: id => dispatch(employeeShowEnter(id)),
  employeeSelect: id => dispatch(employeeSelect(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeShowPage);
