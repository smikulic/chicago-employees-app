import { connect } from 'react-redux';
import { employeeCreate } from '../actions/employee-actions';
import EmployeeCreatePage from '../../pages/employee-create-page';

const mapStateToProps = () => ({})

const mapDispatchToProps = dispatch => ({
  employeeCreate: params => dispatch(employeeCreate(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeCreatePage);
