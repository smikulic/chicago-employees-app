import { connect } from 'react-redux';
import {
  employeeShowEnter,
  employeeFilterBy,
} from '../actions/employee-actions';
import EmployeeIndexPage from '../../pages/employee-index-page';
import { filteredOrInitial } from '../../lib/utilities';

const mapStateToProps = state => ({
  employees: filteredOrInitial(state.appState.filteredEmployees, state.appState.employees),
  employeeSelected: state.appState.employeeSelected,
})

const mapDispatchToProps = dispatch => ({
  employeeShowEnter: id => dispatch(employeeShowEnter(id)),
  employeeFilterBy: (filter, category) => dispatch(employeeFilterBy(filter, category)),
})

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeIndexPage);
