import { employeesMock } from '../redux-test-helper';
import { filteredOrInitial } from './utilities';

describe('Utilities', () => {

  describe('filteredOrInitial function', () => {
    let initialState = employeesMock;
    let filteredState = [];

    describe('when there is no active filter', () => {
      it('returns the initial list state', () => {
        const expectedState = employeesMock;
        expect(filteredOrInitial(filteredState, initialState)).toEqual(expectedState);
      });
    });
    describe('when there is an active filter', () => {
      it('returns the initial list state', () => {
        const expectedState = [employeesMock[0]];
        filteredState = [employeesMock[0]];
        expect(filteredOrInitial(filteredState, initialState)).toEqual(expectedState);
      });
    });
  });
});
