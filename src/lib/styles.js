export const RootStyle = {
  flexGrow: 1,
}

export const TableStyle = {
  minWidth: 460,
}

export const PaperStyle = theme => ({
  marginTop: '2rem',
  padding: theme.spacing.unit * 2,
  textAlign: 'center',
  color: theme.palette.text.secondary,
});

export const TitleStyle = theme => ({
  margin: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 2}px`,
});
