export const filteredOrInitial = (filteredEmployees, employees)  => {
  return (filteredEmployees.length > 0) ? filteredEmployees : employees;
}
